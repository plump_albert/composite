module.exports = {
	content: ["./pages/**/*.{ts,tsx}", "./components/**/*.{ts,tsx}"],
	theme: {
		extend: {},
		fontFamily: {
			serif: ["Lora", "ui-serif", "serif"],
			sans: ["Roboto", "ui-sans-serif", "sans-serif"],
			mono: ["Inconsolata", "monospace", "ui-monospace"],
		},
	},
	plugins: [],
};
