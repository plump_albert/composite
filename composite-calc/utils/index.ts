/**
 * Gets last item from the array
 *
 * @param {any[]} array - source array
 * @returns {any} Returns last item from specified array
 */
export function getLastItem(array: any[]) {
	return array.length > 0 ? array[array.length - 1] : null;
}
