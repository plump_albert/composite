// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "./IComponent";
import NumberNode from "./NumberNode";
import VariableNode from "./VariableNode";
import {
	Sum,
	Multiply,
	Subtract,
	Power,
	Logarithm,
	Divide,
} from "./operations/binary";
import {getLastItem} from ".";

class PrefixNotationTree {
	private root: IComponent = null;

	//#region Private Static methods

	private static isOperation(s: string): IComponent | null {
		switch (s) {
			case "+":
				return new Sum();
			case "-":
				return new Subtract();
			case "*":
				return new Multiply();
			case "/":
				return new Divide();
			case "^":
				return new Power();
			case "log":
				return new Logarithm();
			case "~":
				throw new Error(`Not implemented: '${s}'`);
		}
		return null;
	}

	private static isNumber(s: string): boolean {
		return /^\d[\d,.]*$/.test(s);
	}

	private static isVariable(s: string): boolean {
		return /^[A-z][\w_]*$/.test(s);
	}

	private static recurseParsing(
		equation: string,
		root?: IComponent
	): [IComponent, number] {
		let startIndex = -1;
		let endIndex = -1;
		let node: IComponent;
		for (let i = 0; i < equation.length; ++i) {
			if (startIndex === -1) startIndex = i;
			let subEquation = equation.slice(startIndex, i + 1);
			// If substring is variable or number -- keep iterating
			if (
				PrefixNotationTree.isNumber(subEquation) ||
				PrefixNotationTree.isVariable(subEquation)
			) {
				endIndex = i + 1;
				continue;
			}
			let newNode: IComponent;
			// If we iterate over some characters -- extract node
			if (endIndex !== -1) {
				subEquation = equation.slice(startIndex, endIndex);
				i = endIndex - 1;
				if (PrefixNotationTree.isVariable(subEquation)) {
					newNode = PrefixNotationTree.isOperation(subEquation);
					// If not operation -- create variable node
					if (!newNode) {
						newNode = new VariableNode(subEquation);
					}
					// If operation -- recusively create tree
					else {
						let [operationSubTree, newIndex] =
							PrefixNotationTree.recurseParsing(
								equation.slice(endIndex + 1),
								newNode
							);
						newNode = operationSubTree;
						i = endIndex + newIndex + 1;
					}
				}
				// if it's number
				else {
					newNode = new NumberNode(Number(subEquation));
				}
			} else if (PrefixNotationTree.isOperation(subEquation)) {
				let [operationSubTree, newIndex] = PrefixNotationTree.recurseParsing(
					equation.slice(i + 1),
					PrefixNotationTree.isOperation(subEquation)
				);
				i += newIndex + 1;
				newNode = operationSubTree;
			} else if (subEquation === "(") {
				let openParenthesisIndex: number[] = [i];
				let closeParenthesisIndex: number[] = [equation.indexOf(")", i)];
				openParenthesisIndex.push(equation.indexOf("(", i + 1));
				while (
					getLastItem(openParenthesisIndex) !== -1 &&
					getLastItem(openParenthesisIndex) < getLastItem(closeParenthesisIndex)
				) {
					closeParenthesisIndex.push(
						equation.indexOf(")", getLastItem(closeParenthesisIndex) + 1)
					);
					openParenthesisIndex.push(
						equation.indexOf("(", getLastItem(openParenthesisIndex) + 1)
					);
				}
				if (openParenthesisIndex.length - 1 !== closeParenthesisIndex.length) {
					throw new Error("Unterminated parenthesis");
				}
				let [parenthesisTree, _] = PrefixNotationTree.recurseParsing(
					equation.slice(i + 1, getLastItem(closeParenthesisIndex)),
					null
				);
				newNode = parenthesisTree;
				i = getLastItem(closeParenthesisIndex);
			}
			if (newNode) {
				if (root) {
					// Add new node to existing tree root
					root.add(newNode);
					// If root node is full -- exit
					if (root.isFull()) return [root, i];
				} else if (node) {
					node.add(newNode);
					if (node.isFull()) return [node, i];
				} else {
					if (newNode.isFull()) {
						return [newNode, i];
					}
					node = newNode;
				}
			}
			startIndex = -1;
			endIndex = -1;
		}
		if (endIndex !== -1) {
			let subEquation = equation.slice(startIndex, endIndex);
			let newNode: IComponent;
			if (PrefixNotationTree.isVariable(subEquation)) {
				newNode = PrefixNotationTree.isOperation(subEquation);
				if (!newNode) newNode = new VariableNode(subEquation);
			} else if (PrefixNotationTree.isOperation(subEquation)) {
				newNode = PrefixNotationTree.isOperation(subEquation);
			} else {
				newNode = new NumberNode(Number(subEquation));
			}
			if (root) root.add(newNode);
			else if (node) node.add(newNode);
			else node = newNode;
		}
		return root ? [root, equation.length - 1] : [node, equation.length - 1];
	}

	//#endregion

	static parse(equation: string): PrefixNotationTree {
		let tree = new PrefixNotationTree();
		tree.root = PrefixNotationTree.recurseParsing(equation)[0];
		return tree;
	}

	//#region Public Methods

	public getRoot(): IComponent {
		return this.root;
	}

	public toString(): string {
		return this.root.toString();
	}

	public evaluate(): number {
		return this.root.evaluate();
	}

	public getVariables(): VariableNode[] {
		return this.root?.getVariables() || [];
	}

	//#endregion
}

export default PrefixNotationTree;
