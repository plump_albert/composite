// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "./IComponent";
import VariableNode from "./VariableNode";

class NumberNode implements IComponent {
	private value: number;
	priority: number = 0;

	constructor(value: number) {
		this.value = value;
	}
	getNodes(): IComponent[] {
		return [];
	}
	getVariables(): VariableNode[] {
		return [];
	}
	isFull(): boolean {
		return true;
	}
	toString(): string {
		return this.value.toString();
	}
	add(node: IComponent): IComponent {
		this.value = node.evaluate();
		return this;
	}
	getName(): string {
		return this.value == undefined ? '' : this.value.toString();
	}

	setValue(value: number): NumberNode {
		this.value = value;
		return this;
	}

	evaluate(): number {
		return this.value;
	}
}

export default NumberNode;
