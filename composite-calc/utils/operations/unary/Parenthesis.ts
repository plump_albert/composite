// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import UnaryOperation from "./UnaryOperation";

class Parenthesis extends UnaryOperation {
	getName(): string {
	    return '()';
	}
	toString(): string {
		return `(${this.node})`;
	}
}

export default Parenthesis;
