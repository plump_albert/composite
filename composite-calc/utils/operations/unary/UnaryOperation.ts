// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "../../IComponent";
import VariableNode from "../../VariableNode";

abstract class UnaryOperation implements IComponent {
	//#region Interface implementation
	priority: number = 10;
	evaluate(): number {
		if (!this.node) throw new Error("Node is not defined");
		return this.node.evaluate();
	}

	add(node: IComponent): IComponent {
		this.node = node;
		return this;
	}

	isFull(): boolean {
		return !!this.node;
	}

	getVariables(): VariableNode[] {
		return this.node ? this.node.getVariables() : [];
	}

	getNodes(): IComponent[] {
		return [this.node];
	}

	abstract toString(): string;
	abstract getName(): string;

	//#endregion

	//#region Class properties

	protected node: IComponent;

	//#endregion
}

export default UnaryOperation;
