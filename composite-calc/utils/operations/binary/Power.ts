// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "../../IComponent";
import BinaryOperation from "./BinaryOperation";

class Power extends BinaryOperation {
	priority = 3;
	getName(): string {
	    return '^';
	}
	toString(): string {
		return `${this.leftHandSideToString()}^${this.rightHandSideToString()}`;
	}
	setBase(value: IComponent): Power {
		this.setLeftHandSide(value);
		return this;
	}
	setDegree(value: IComponent): Power {
		this.setRightHandSide(value);
		return this;
	}
	evaluate(): number {
		if (!this.isFull()) throw new Error("Not enough arguments");
		return Math.pow(this.lhs.evaluate(), this.rhs.evaluate());
	}
}

export default Power;
