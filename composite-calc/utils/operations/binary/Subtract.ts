// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import BinaryOperation from "./BinaryOperation";

class Subtract extends BinaryOperation {
	getName(): string {
	    return '-';
	}
	toString(): string {
		return `${this.leftHandSideToString()}-${this.rightHandSideToString()}`;
	}
	evaluate(): number {
		if (!this.isFull()) throw new Error("Not enough arguments");
		return this.lhs.evaluate() - this.rhs.evaluate();
	}
}

export default Subtract;
