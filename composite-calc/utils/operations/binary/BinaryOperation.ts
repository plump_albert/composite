// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "../../IComponent";
import NumberNode from "../../NumberNode";
import VariableNode from "../../VariableNode";

abstract class BinaryOperation implements IComponent {
	priority: number = 1;
	protected lhs: IComponent;
	protected rhs: IComponent;

	abstract evaluate(): number;
	abstract getName(): string;
	abstract toString(): string;
	add(node: IComponent): IComponent {
		if (!this.lhs) this.setLeftHandSide(node);
		else if (!this.rhs) this.setRightHandSide(node);
		else throw new Error("Can't add new nodes");
		return this;
	}
	isFull(): boolean {
		return !!this.lhs && !!this.rhs;
	}
	getVariables(): VariableNode[] {
		if (!this.lhs || !this.rhs) return [];
		return [...this.lhs.getVariables(), ...this.rhs.getVariables()];
	}
	getNodes(): IComponent[] {
		return [this.lhs, this.rhs];
	}

	setLeftHandSide(value: IComponent): BinaryOperation {
		this.lhs = value;
		return this;
	}

	setRightHandSide(value: IComponent): BinaryOperation {
		this.rhs = value;
		return this;
	}

	protected leftHandSideToString() {
		if (this.lhs instanceof NumberNode || this.lhs instanceof VariableNode) {
			return this.lhs.toString();
		} else if (this.lhs.priority < this.priority) {
			return `(${this.lhs})`;
		} else {
			return this.lhs.toString();
		}
	}

	protected rightHandSideToString() {
		if (this.rhs instanceof NumberNode || this.rhs instanceof VariableNode) {
			return this.rhs.toString();
		} else if (this.rhs.priority < this.priority) {
			return `(${this.rhs})`;
		} else {
			return this.rhs.toString();
		}
	}
}

export default BinaryOperation;
