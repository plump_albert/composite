// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "../../IComponent";
import BinaryOperation from "./BinaryOperation";

class Logarithm extends BinaryOperation {
	priority = 3;
	getName(): string {
	    return 'log';
	}
	toString(): string {
		let left = this.leftHandSideToString().replace("(", "").replace(")", "");
		return `log_{${left}}(${this.rhs})`;
	}
	setBase(value: IComponent): Logarithm {
		this.setLeftHandSide(value);
		return this;
	}

	setValue(value: IComponent): Logarithm {
		this.setRightHandSide(value);
		return this;
	}

	evaluate(): number {
		if (!this.isFull()) throw new Error("Not enough arguments");
		return Math.log(this.lhs.evaluate()) / Math.log(this.rhs.evaluate());
	}
}

export default Logarithm;
