// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import IComponent from "./IComponent";

class VariableNode implements IComponent {
	private value: IComponent;
	name: string;
	priority: number = 0;

	public constructor(name: string) {
		if (name.length === 0) {
			throw new Error("Name cannot be empty");
		}
		this.name = name;
	}

	//#region IComponent methods
	evaluate(): number {
		if (!this.value) return null;
		return this.value.evaluate();
	}
	add(node: IComponent): IComponent {
		this.value = node;
		return this;
	}
	isFull(): boolean {
		return true;
	}
	toString(): string {
		return this.name.toString();
	}
	getVariables(): VariableNode[] {
		return [this];
	}
	getNodes(): IComponent[] {
		return [];
	}
	getName(): string {
		return this.value ? `${this.name} = ${this.value.evaluate()}` : this.name;
	}
	//#endregion

	public setValue(value: IComponent): IComponent {
		return this.add(value);
	}
}

export default VariableNode;
