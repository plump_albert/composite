// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import VariableNode from "./VariableNode";

interface IComponent {
	priority: number;
	evaluate(): number;
	add(node: IComponent): IComponent;
	isFull(): boolean;
	toString(): string;
	getVariables(): VariableNode[];
	getNodes(): IComponent[];
	getName(): string;
}

export default IComponent;
