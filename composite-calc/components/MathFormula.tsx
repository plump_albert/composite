// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import React from "react";
import {MathJax, MathJaxContext} from "better-react-mathjax";

interface IProps {
	className?: string;
	equation: string;
	onError?: (error: any) => void;
	onSuccess?: () => void;
}

const MathFormula: React.FC<IProps> = ({
	className,
	equation,
	onError,
	onSuccess,
}) => {
	return (
		<MathJaxContext version={3} onError={onError} config={{renderMode: "post"}}>
			<MathJax dynamic onEnded={onSuccess} className={className}>
				{`\\(${equation}\\)`}
			</MathJax>
		</MathJaxContext>
	);
};

export default MathFormula;
