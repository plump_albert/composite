// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import React, {useState} from "react";

interface IProps {
	className?: string;
	placeholder?: string;
	value?: string;
	onChange?: React.ChangeEventHandler<HTMLInputElement>;
}

const Input: React.FC<IProps> = ({className, placeholder, value, onChange}) => {
	const [isFocused, setFocus] = useState(false);
	return (
		<span
			className={`${className || ""} input ${
				isFocused ? "input--focused" : ""
			}`}
		>
			<input
				className={"input-root"}
				placeholder={placeholder}
				value={value}
				onChange={onChange}
				onFocus={() => setFocus(true)}
				onBlur={() => setFocus(false)}
			></input>
		</span>
	);
};

export default Input;
