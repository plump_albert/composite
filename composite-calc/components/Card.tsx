// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import React from "react";

interface IProps {
	containerClassName?: string;
	className?: string;
	header: string;
}

const Card = React.forwardRef<HTMLDivElement, React.PropsWithChildren<IProps>>(
	(
		{containerClassName, className, header, children},
		ref: React.ForwardedRef<HTMLDivElement>
	) => (
		<div
			className={`bg-white dark:bg-slate-800 text-black dark:text-white flex flex-col items-baseline shadow-lg dark:ring-1 dark:ring-white/10 dark:ring-inset py-4 px-6 xl:py-8 xl:px-16 rounded ${containerClassName}`}
		>
			<h1 className="w-full text-2xl uppercase leading-normal text-center font-serif font-bold xl:text-5xl xl:text-left">
				{header}
			</h1>
			<div
				ref={ref}
				className={`flex flex-col items-center justify-left empty:mt-0 mt-8 w-full ${className}`}
			>
				{children}
			</div>
		</div>
	)
);
Card.displayName = "Card";

export default Card;
