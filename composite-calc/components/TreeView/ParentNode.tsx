import {Group} from "@visx/group";
import {INodeProps} from ".";

interface ParentNodeProps {
	radius: number;
}

const ParentNode: React.FC<INodeProps & ParentNodeProps> = ({node, radius}) => {
	return (
		<Group top={node.y} left={node.x}>
			<circle r={radius} className="fill-blue-700 dark:fill-amber-600" />
			<text
				dy=".33em"
				textAnchor="middle"
				className="fill-white dark:fill-slate-900 font-bold"
				style={{pointerEvents: "none"}}
			>
				{node.data.getName()}
			</text>
		</Group>
	);
};

export default ParentNode;
