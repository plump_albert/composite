// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import {Group} from "@visx/group";
import {hierarchy, Tree} from "@visx/hierarchy";
import {HierarchyPointNode} from "@visx/hierarchy/lib/types";
import {LinkVerticalLine} from "@visx/shape";
import {useMemo} from "react";

import IComponent from "../../utils/IComponent";
import Node from "./Node";

interface IProps {
	className?: string;
	node: IComponent;
	width: number;
}

export interface INodeProps {
	node: HierarchyPointNode<IComponent>;
}

const TreeView: React.FC<IProps> = ({className, node, width}) => {
	const margin = 16;
	const root = useMemo(() => hierarchy(node, n => n?.getNodes()), [node]);
	const height = (root.height + 1) * 64;
	return (
		<svg
			className={`text-xs font-mono h-max flex-1 ${className || ""}`}
			width={width}
			viewBox={`0 0 ${width} ${height}`}
			preserveAspectRatio="xMidYMid meet"
		>
			<Tree
				root={root}
				size={[width - 4 * margin, height - 4 * margin]}
				separation={(a, b) => (a.parent === b.parent ? 1 : 0.25) / a.depth}
			>
				{tree => (
					<Group top={margin} left={margin}>
						<Group top={margin / 2}>
							{tree.links().map((link, i) => (
								<LinkVerticalLine
									key={`tree-view-node-${i}`}
									data={link}
									className="stroke-gray-400/30 dark:stroke-lime-200/30"
									strokeWidth="2"
									fill="none"
								/>
							))}
						</Group>
						<Group top={margin / 2}>
							{tree.descendants().map((node, key) => {
								return (
									<Node
										node={node}
										width={2 * margin}
										height={1.5 * margin}
										key={key}
									></Node>
								);
							})}
						</Group>
					</Group>
				)}
			</Tree>
		</svg>
	);
};

export default TreeView;
