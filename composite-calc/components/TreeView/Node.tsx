import {Group} from "@visx/group";
import {INodeProps} from ".";
import ParentNode from "./ParentNode";

interface NodeProps {
	width: number;
	height: number;
}

const Node: React.FC<INodeProps & NodeProps> = ({node, width, height}) => {
	if (!!node.children)
		return <ParentNode radius={Math.min(width, height) / 1.5} node={node} />;

	const text = node.data?.getName();
	const centerY = -height / 2;
	const calcWidth = text ? text.replaceAll(/\s/g, '').length * 12 : width;
	const centerX = -Math.max(width, calcWidth) / 2;

	return (
		<Group top={node.y} left={node.x}>
			<rect
				height={height}
				width={Math.max(width, calcWidth)}
				y={centerY}
				x={centerX}
				className="fill-white stroke-blue-700 dark:fill-slate-800 dark:stroke-amber-500/80"
				strokeWidth={1}
				strokeDasharray="2,2"
				strokeOpacity={0.6}
				rx={10}
			/>
			<text
				dy=".33em"
				className="font-mono fill-blue-900 dark:fill-white"
				textAnchor="middle"
				style={{pointerEvents: "none"}}
			>
				{text}
			</text>
		</Group>
	);
};

export default Node;
