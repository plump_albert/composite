// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import {useCallback, useEffect, useState} from "react";
import PrefixNotationTree from "../utils/PrefixNotationTree";
import VariableNode from "../utils/VariableNode";
import Input from "./Input";
import MathFormula from "./MathFormula";

interface IProps {
	node: VariableNode;
	className?: string;
	onChange?: () => void;
}

const VariableInput: React.FC<IProps> = ({className, node, onChange}) => {
	const [equation, setEquation] = useState("");
	const [value, setValue] = useState<number>();

	const handleEquationChange = useCallback(
		e => {
			setEquation(e.target.value);
			let tree = PrefixNotationTree.parse(e.target.value);
			node.setValue(tree.getRoot());
			try {
				setValue(node.evaluate());
			} catch (e) {
				setValue(undefined);
			} finally {
				onChange();
			}
		},
		[node, onChange]
	);

	useEffect(() => {
		handleEquationChange({target: {value: equation}});
	}, [handleEquationChange, equation]);

	return (
		<p className={`flex max-w-fit w-full items-center ${className}`}>
			<MathFormula className="flex-initial mr-2" equation={`${node.name}=`} />
			<Input
				className="max-w-fit flex-1 px-2 py-2"
				value={equation}
				onChange={handleEquationChange}
			/>
			{value != undefined ? (
				<MathFormula className="ml-2" equation={`=${value}`} />
			) : (
				<span
					title="Cannot evaluate expression"
					className="material-icons ml-2 text-red-500"
				>
					error
				</span>
			)}
		</p>
	);
};

export default VariableInput;
