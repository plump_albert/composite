// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import NumberNode from "../utils/NumberNode";
import {Sum} from "../utils/operations/binary";
import PrefixNotationTree from "../utils/PrefixNotationTree";
import VariableNode from "../utils/VariableNode";

describe("parsing single node", () => {
	it("should parse number", () => {
		let equation = "5";
		const tree = PrefixNotationTree.parse(equation);
		expect(tree.getRoot()).toBeInstanceOf(NumberNode);
	});

	it("should parse variable", () => {
		let equation = "x";
		const tree = PrefixNotationTree.parse(equation);
		expect(tree.getRoot()).toBeInstanceOf(VariableNode);
	});

	it("should parse operation", () => {
		let equation = "+5 6";
		const tree = PrefixNotationTree.parse(equation);
		expect(tree.getRoot()).toBeInstanceOf(Sum);
	});
});

describe("parsing equations", () => {
	test("simple equation", () => {
		let tree = PrefixNotationTree.parse("+3*-4 54 3");
		expect(tree.toString()).toBe("3+(4-54)*3");
	});

	test("equation with variables", () => {
		let tree = PrefixNotationTree.parse("-x+5 3");
		expect(tree.toString()).toBe("x-5+3");
	});

	test("expect equation with parenthesis", () => {
		let tree = PrefixNotationTree.parse("*x+5 3");
		expect(tree.toString()).toBe("x*(5+3)");
	});

	test("source equation with parenthesis", () => {
		let tree = PrefixNotationTree.parse("*x(+5 3)");
		expect(tree.toString()).toBe("x*(5+3)");
	});

	test("complex equation", () => {
		let tree = PrefixNotationTree.parse("+3*log 4 54 3");
		expect(tree.toString()).toBe("3+log_{4}(54)*3");
	});
});

test("evaluates tree", () => {
	let tree = PrefixNotationTree.parse("+5*6 7");
	expect(tree.evaluate()).toBe(47);
});
