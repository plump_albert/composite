// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import {AppProps} from "next/app";
import "../styles/globals.scss";

function App({Component, pageProps}: AppProps) {
	return <Component {...pageProps} />;
}

export default App;
