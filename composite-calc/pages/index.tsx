// vim:ft=typescriptreact:ts=2:sw=0
// Author: Plump Albert (plumpalbert@gmail.com)
import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import Head from "next/head";
// Custom components
import Card from "../components/Card";
import Input from "../components/Input";
import MathFormula from "../components/MathFormula";
import TreeView from "../components/TreeView";
import VariableInput from "../components/VariableInput";
import PrefixNotationTree from "../utils/PrefixNotationTree";

function Index() {
	const [equation, setEquation] = useState("");
	const [evaluate, setEvaluate] = useState(false);
	const [equationResult, setEquationResult] = useState<number>(undefined);
	useEffect(() => {
		if (!evaluate) return;
		try {
			setEquationResult(equationTree.evaluate());
		} catch (err) {
			setEquationResult(undefined);
		} finally {
			setEvaluate(false);
		}
	}, [evaluate]);

	const handleInputChange = useCallback<
		React.ChangeEventHandler<HTMLInputElement>
	>(e => {
		setEquation(e.target.value);
		setEquationResult(undefined);
	}, []);

	const equationTree = useMemo(() => {
		const tree = PrefixNotationTree.parse(equation);
		if (tree.getRoot()) setEvaluate(true);
		return tree;
	}, [equation]);

	const infixEquation = useMemo(() => {
		try {
			return equationTree.toString();
		} catch (err) {
			return "Oooops!";
		}
	}, [equationTree]);

	const dispatchEvaluate = useCallback(() => setEvaluate(true), [setEvaluate]);

	const variables = useMemo(
		() =>
			equationTree
				.getVariables()
				.map((v, i) => (
					<VariableInput
						key={`variable-input-${i}`}
						node={v}
						onChange={dispatchEvaluate}
					/>
				)),
		[equationTree]
	);

	const treeCardRef = useRef<HTMLDivElement>(undefined);

	return (
		<div className="flex flex-1 flex-col gap-6 h-full w-full px-4 py-6 xl:px-8 xl:py-12 bg-slate-300 dark:bg-slate-900 overflow-x-hidden overflow-y-auto">
			<Head>
				<title>Composite calculator</title>
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Card
				className="flex flex-col items-baseline"
				header="Математическое выражение"
			>
				<Input
					className="font-mono w-[calc(100%+1rem)] mx-[-0.5rem] xl:w-[calc(100%+2rem)] xl:mx-[-1rem]"
					placeholder="Введите математическое выражение в польской нотации"
					onChange={handleInputChange}
				/>
				<h2 className="flex text-2xl font-serif font-bold mt-6 mb-4 self-center xl:self-start">
					Инфиксная запись
				</h2>
				<MathFormula
					className="self-center xl:self-start"
					equation={`${infixEquation}=${equationResult}`}
				/>
			</Card>
			<div className="flex flex-1 flex-col gap-6 xl:flex-row">
				<Card
					className="flex flex-col gap-4 w-fit items-center"
					header="Переменные"
				>
					{variables}
				</Card>
				<Card
					containerClassName="w-full"
					ref={treeCardRef}
					className="flex-auto items-center"
					header="Дерево вычислений"
				>
					{equationResult != undefined && (
						<TreeView
							width={treeCardRef.current?.clientWidth}
							node={equationTree.getRoot()}
						/>
					)}
				</Card>
			</div>
		</div>
	);
}

export default Index;
